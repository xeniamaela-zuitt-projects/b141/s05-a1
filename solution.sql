1. Return the customerName of the customers who are from the Philippines.
Answer:
SELECT customerName FROM customers WHERE country = 'Philippines';

2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
Answer:
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

3. Return the product name and MSRP of the product named "The Titanic".
Answer:
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com".
Answer:
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

5. Return the names of customers who have no registered state.
Answer:
SELECT customerName FROM customers WHERE state is NULL;

6. Return the first name, last name, email of the employee whose last name is patterson and first name is Steve.
Answer:
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000.
Answer:
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA"
AND creditLimit > 3000;

8. Return the customer names of customers whose customer names dont have 'a' in them.
Answer:
SELECT customerName FROM customers WHERE customerName NOT LIKE "%a%";

9. Return the customer numbers of orders whose comments contain the string 'DHL'.
Answer:
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

10. Return the product lines whose text description mentions the phrase 'state of the art'.
Answer:
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

11. Return the countries of customers without duplication.
Answer:
SELECT DISTINCT country FROM customers;

12. Return the statuses of orders without duplication.
SELECT DISTINCT status FROM orders;

13. Return the customer names and countries of customers whose country is USA, FRANCE, or CANADA.
Answer:
SELECT customerName, country FROM customers WHERE country = "USA" OR country = "FRANCE" OR country = "CANADA";

14. Return the first name, last name, and office's city of employees whose offices are in Tokyo.
Answer:
SELECT employees.firstName, employees.lastName, offices.city
FROM employees JOIN offices ON employees.officeCode = offices.officeCode
WHERE city = "Tokyo";

15. Return the customer names of customers who were served by the employee named "Leslie Thompson".
Answer:
SELECT customers.customerName FROM customers JOIN employees
ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE firstName = "Leslie" AND lastName = "Thompson";

16. Return the product names and customer name of products ordered by "Baanne Mini Imports"
Answer: 
SELECT products.productName, customers.customerName
FROM orders JOIN customers ON orders.orderNumber = customers.customerNumber
JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
JOIN products ON orderdetails.productCode = products.productCode
WHERE customerName = "Baanne Mini Imports";

17. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country.
Answer:
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country
FROM employees JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
JOIN offices ON employees.officeCode = offices.officeCode
WHERE customers.country = offices.country;

18. Return the last names and first names of employess being supervised by "Anthony Bow".
Answer:
SELECT lastName, firstName FROM employees
WHERE reportsTo = (SELECT employeeNumber FROM employees WHERE firstName = "Anthony" AND lastName = "Bow");


19. Return the product name and MSRP of the product with the highest MSRP.
Answer: 
SELECT productName, MAX(MSRP) FROM products;

20. Return the number of customers in the UK
Answer: 
SELECT COUNT(customerName) AS customer_count FROM customers WHERE country = "UK";

21. Return the number of products per product line.
Answer: 
SELECT productLine, COUNT(productLine) AS numberOfProductLine FROM products GROUP BY productLine;

22. Return the number of customers served by every employee
SELECT COUNT(customers.customerNumber) AS customer_count,
CONCAT(employees.firstName, ' ', employees.lastName) AS employee_name
FROM customers JOIN employees
ON customers.salesRepEmployeeNumber = employees.employeeNumber
GROUP BY customers.salesRepEmployeeNumber;

23. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000.
Answer:
SELECT productName, quantityInStock FROM products
WHERE productLine = "planes" AND quantityInStock < 1000;